# Test-Mundo-Free



## General
Este prueba se desarrollo con nodeJs-express para el backend y se realizó el despliegue en heroku; el desarrollo de la app se realizó con Android Studio-java y retrofit para consumo de los sericios de backend. El repositorio tiene:

- Carpeta con el codigo fuente de android studio.
- Carpeta con el codigo fuente del backend.
- APK de la app desarrollada.
- Archivo postman con pruebas del backend.


## Documentación API

```
- Url: https://backtestmundofree.herokuapp.com/api/v1/favorities/{userId}
- Metodo: GET
- Descripción: Servicio para obtener todos las listas de deseos y sus respectivos productos dado el id de un usuario {userId}.
```

```
- Url: https://backtestmundofree.herokuapp.com/api/v1/favorities/
- Metodo: POST
- Descripción: Servicio para crear una listas de deseos y asociarla a un usuario. Un usuario puede tener diferentes listas de deseo.
- Parametro de entrada: 
{
  "userId": Number,
  "name": String,
  "products":[
    {
      "name": String,
      "price": String,
      "imagen": String
    }
  ]
}
```

```
- Url: https://backtestmundofree.herokuapp.com/api/v1/favorities/
- Metodo: PUT
- Descripción: Servicio para actualizar una listas de deseos dado el Id del usuario {userId} y el nombre de la lista {name}.
- Parametro de entrada: 
{
  "userId": Number,
  "name": String,
  "products":[
    {
      "name": String,
      "price": String,
      "imagen": String
    }
  ]
}
```

## Modelo de respuesta de las APIS:
{
   "success": boolean,
   "result": Object,
   "message": String
}

- Campo success: indica si fue exitosa la ejecución.
- Campo result: devuelve un objeto como resultado según el servicio.
- message: indica un mensaje de ayuda en caso de error.