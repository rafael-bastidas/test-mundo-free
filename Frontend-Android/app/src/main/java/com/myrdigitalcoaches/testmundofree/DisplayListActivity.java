package com.myrdigitalcoaches.testmundofree;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.myrdigitalcoaches.testmundofree.adapters.ListFavoritiesAdapter;
import com.myrdigitalcoaches.testmundofree.api.ApiAdapter;
import com.myrdigitalcoaches.testmundofree.objets.Favorit;
import com.myrdigitalcoaches.testmundofree.objets.GenericResponseListFavorit;
import com.myrdigitalcoaches.testmundofree.objets.GenericResponseString;
import com.myrdigitalcoaches.testmundofree.objets.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayListActivity extends AppCompatActivity {

    ListFavoritiesAdapter adapterProducts;
    ArrayAdapter<String> arrayAdapter;
    List<Favorit> favorities = new ArrayList<>();
    Integer itemNameSelected = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_list);

        Intent intent = getIntent();
        Integer userId = intent.getIntExtra("userId",-1);


        Spinner spinner = findViewById(R.id.sp_names);
        RecyclerView listProducts = findViewById(R.id.rv_favorities);
        EditText etName = findViewById(R.id.et_name);
        EditText etPrice = findViewById(R.id.et_price);
        Button btnAddProduct = findViewById(R.id.btn_addProduct);


        listProducts.setLayoutManager(new LinearLayoutManager(this));

        Call<GenericResponseListFavorit> call = ApiAdapter.getApiService().getFavoritiesById(userId);
        call.enqueue(new Callback<GenericResponseListFavorit>() {
            @Override
            public void onResponse(Call<GenericResponseListFavorit> call, Response<GenericResponseListFavorit> response) {
                GenericResponseListFavorit resp = response.body();
                if (resp != null) {
                    if(resp.getSuccess()){
                        favorities = resp.getResult();
                        arrayAdapter = new ArrayAdapter<>(DisplayListActivity.this, android.R.layout.simple_spinner_dropdown_item, getListNameFavorities(favorities));
                        spinner.setAdapter(arrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericResponseListFavorit> call, Throwable t) { }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                itemNameSelected = i;
                adapterProducts = new ListFavoritiesAdapter(getProducts(favorities, arrayAdapter.getItem(i)));
                listProducts.setAdapter(adapterProducts);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        btnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Favorit favorit = new Favorit();
                favorit.setName(arrayAdapter.getItem(itemNameSelected));
                favorit.setUserId(userId);
                List<Product> products = new ArrayList<Product>();
                Product product = new Product(etName.getText().toString(), etPrice.getText().toString(), null);
                products.add(product);
                favorit.setProducts(products);
                Call<GenericResponseString> call = ApiAdapter.getApiService().updateProducts(favorit);
                call.enqueue(new Callback<GenericResponseString>() {
                    @Override
                    public void onResponse(Call<GenericResponseString> call, Response<GenericResponseString> response) {
                        etName.setText("");
                        etPrice.setText("");
                        Toast.makeText(getApplicationContext(),"Save", Toast.LENGTH_LONG).show();
                        adapterProducts = new ListFavoritiesAdapter(addProduct(favorities, arrayAdapter.getItem(itemNameSelected), product));
                        listProducts.setAdapter(adapterProducts);
                    }

                    @Override
                    public void onFailure(Call<GenericResponseString> call, Throwable t) {

                    }
                });
            }
        });
    }

    public List<Product> addProduct(List<Favorit> list, String name, Product product){
        for (Favorit favorit : list) {
            if (favorit.getName() == name) {
                favorit.getProducts().add(product);
                return favorit.getProducts();
            }
        }
        return new ArrayList<>();
    }

    public List<Product> getProducts(List<Favorit> list, String name){
        for (Favorit favorit : list) {
            if (favorit.getName() == name) {
                return favorit.getProducts();
            }
        }
        return new ArrayList<>();
    }

    public List<String> getListNameFavorities(List<Favorit> list){
        List<String> items = new ArrayList<String>();
        for (Favorit favorit : list) {
            items.add(favorit.getName());
        }

        return items;
    }
}