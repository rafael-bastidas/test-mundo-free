package com.myrdigitalcoaches.testmundofree.objets;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenericResponseString {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private String result;
    @SerializedName("message")
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
