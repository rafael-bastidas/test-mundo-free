package com.myrdigitalcoaches.testmundofree.objets;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenericResponseListFavorit {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("result")
    private List<Favorit> result;
    @SerializedName("message")
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Favorit> getResult() {
        return result;
    }

    public void setResult(List<Favorit> result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
