package com.myrdigitalcoaches.testmundofree.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.myrdigitalcoaches.testmundofree.R;
import com.myrdigitalcoaches.testmundofree.objets.Product;

import java.util.List;

public class ListFavoritiesAdapter extends RecyclerView.Adapter<ListFavoritiesAdapter.FavoritiesViewHolder> {

    List<Product> products;

    public ListFavoritiesAdapter(List<Product> products) {
        this.products = products;
    }

    @NonNull
    @Override
    public FavoritiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorities, null, false);
        return new FavoritiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListFavoritiesAdapter.FavoritiesViewHolder holder, int position) {
        holder.title.setText(products.get(position).getName().toUpperCase());
        holder.subTitle.setText("Precio: " + products.get(position).getPrice());
        holder.imgUrl.setImageResource(R.drawable.buy);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class FavoritiesViewHolder extends RecyclerView.ViewHolder {

        TextView title, subTitle;
        ImageView imgUrl;

        public FavoritiesViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tv_title);
            subTitle = itemView.findViewById(R.id.tv_subtitle);
            imgUrl = itemView.findViewById(R.id.iv_img);

        }
    }

}
