package com.myrdigitalcoaches.testmundofree;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private final Integer listUserId[] = {-1,1,2,3};
    private  Integer itemSelected = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = findViewById(R.id.sp_userId);
        Button btnView = findViewById(R.id.btn_view);

        spinner.setPrompt("Selec.");
        btnView.setEnabled(false);

        ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, listUserId);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (arrayAdapter.getItem(i) != -1) {
                    btnView.setEnabled(true);
                    itemSelected = i;
                } else {
                    btnView.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer userId = arrayAdapter.getItem(itemSelected);
                goListFavorities(userId);
            }
        });

    }

    public void goListFavorities(Integer id){
        Intent intent = new Intent(this, DisplayListActivity.class);
        intent.putExtra("userId", id);
        startActivity(intent);
    }
}