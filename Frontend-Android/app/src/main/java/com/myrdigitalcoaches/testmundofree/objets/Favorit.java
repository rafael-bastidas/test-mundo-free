package com.myrdigitalcoaches.testmundofree.objets;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Favorit {
    @SerializedName("userId")
    private Integer userId;
    @SerializedName("name")
    private String name;
    @SerializedName("products")
    private List<Product> products;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
