package com.myrdigitalcoaches.testmundofree.api;

import com.myrdigitalcoaches.testmundofree.objets.Favorit;
import com.myrdigitalcoaches.testmundofree.objets.GenericResponseListFavorit;
import com.myrdigitalcoaches.testmundofree.objets.GenericResponseString;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiService {

    @GET("api/v1/favorities/{id}")
    Call<GenericResponseListFavorit> getFavoritiesById(@Path("id") Integer id);

    @PUT("api/v1/favorities")
    Call<GenericResponseString> updateProducts(@Body Favorit favorit);

}
